INTRODUCTION

tab2imdi is a simple converter from tab (or comma) separated
values into IMDI metadata descriptions*, with all constant values
taken from an IMDI template**.

** The template is produced by some other IMDI tool, such
as arbil https://tla.mpi.nl/tools/tla-tools/arbil/

* IMDI is a metadata standard for linguistic resources.
An xml schema is here http://www.mpi.nl/IMDI/Schema/IMDI_3.0.xsd

For a detailed user guide, see the documentation folder.

INSTALLATION

tab2imdi is simple python-only program with no external dependencies,
except that you will need lxml (http://lxml.de/) for validation
(optional, but recommended).

Just unzip to some directory of your choice and cd into it. After that,

$> ./tab2imdi.py -h

  gives some help;


$> ./tab2imdi.py test/imdi_test_template.imdi test/imdi_test.csv

  writes a number of generated IMDI files to (default) imdi_output
  (you will need to add -w or --write for overwrite, if imdi_output is not
  empty)


$> ./tab2imdi.py test/imdi_test_template.imdi test/imdi_test.tsv \
  --validate --xml-schema=IMDI_3.0.xsd
  does the same thing, but with validation against IMDI_3.0.xsd (included);
  failed validation will issue warnings rather than fatals


$> ./tab2imdi.py test/bad_imdi_test_template.imdi test/imdi_test.csv

  should cause a fatal error due to template field not found in input csv.